FROM adoptopenjdk/openjdk8:alpine
WORKDIR /tmp/compile

COPY gradle ./gradle
COPY *.gradle ./
COPY gradlew ./
COPY src ./src
COPY server/plugins ./plugins

RUN ./gradlew --no-daemon build

FROM openjdk:8-jre-alpine

RUN apk -U --no-cache upgrade
RUN apk add --no-cache -U wget

ADD server /server
COPY --from=0 /tmp/compile/build/libs/*.jar /server/plugins
RUN ["wget", "-O", "/server/spigot.jar", "https://cdn.getbukkit.org/spigot/spigot-1.8.8-R0.1-SNAPSHOT-latest.jar"]

WORKDIR /server
CMD ["java", "-jar", "/server/spigot.jar"]